package devops.kilroywashere.wshabitation.services;

import devops.kilroywashere.wshabitation.models.*;
import devops.kilroywashere.wshabitation.repositories.HabitationItemRepository;
import devops.kilroywashere.wshabitation.repositories.HabitationOptionsRepository;
import devops.kilroywashere.wshabitation.repositories.HabitationTop10Repository;
import devops.kilroywashere.wshabitation.repositories.HabitationsRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Service
public class HabitationsServiceImpl implements HabitationsService {
    private final HabitationsRepository repository;
    private final HabitationTop10Repository habitationTop10Repository;
    private final HabitationItemRepository habitationItemRepository;
    private final HabitationOptionsRepository habitationOptionsRepository;

    // DI par Spring Boot
    public HabitationsServiceImpl(
            HabitationsRepository repository,
            HabitationTop10Repository habitationTop10Repository,
            HabitationItemRepository habitationItemRepository,
            HabitationOptionsRepository habitationOptionsRepository
    ) {
        this.repository = repository;
        this.habitationItemRepository = habitationItemRepository;
        this.habitationOptionsRepository = habitationOptionsRepository;
        this.habitationTop10Repository = habitationTop10Repository;
    }

    /**
     * Retourne toutes les habitations
     *
     * @return Une liste d'objet Habitation
     */
    @Override
    public Iterable<Habitation> findAllHabitations() {
        return repository.findAll();
    }

    @Override
    public Iterable<Habitation> findAllHabitationsTop10() {
        return habitationTop10Repository.findTop10();
    }

    @Override
    public Iterable<Habitation> findAllHabitationsByTypeHabitatId(int id) {
        return repository.findHabitationsByTypehabitat_Id(id);
    }

    @Override
    public Iterable<Habitation> findAllHabitationsByVilleId(String id) {
        return repository.findHabitationsByIdVille(id);
    }

    @Override
    public Iterable<Habitation> findAllHabitationsByIds(List<Integer> ids) {
        return repository.findHabitationsByIdIsIn(ids);
    }

    /*
    https://tech.asimio.net/2020/11/21/Implementing-dynamic-SQL-queries-using-Spring-Data-JPA-Specification-and-Criteria-API.html
    https://medium.com/backend-habit/spring-jpa-make-dynamic-where-using-predicate-and-criteria-84550dfaa182
    https://www.baeldung.com/hibernate-criteria-queries
     */
    @Override
    public Iterable<Habitation> findAllHabitationsByCaracteristique(Map<String, String> paramaters) {
        return repository.findAllHabitationsByCaracteristique(paramaters);
    }

    /** Retourne l'habitation associée à l'id
     *
     * @param id Id de l’habitation
     * @return Un objet Optional contenant l'objet Habitation correspondant à l'id ou
     *         un objet Optional vide
     */
    @Override
    public Habitation findHabitationById(@NotNull Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public long countHabitations() {
        return repository.count();
    }

    /** Ajout d'une habitation
     *
     * @param habitation Objet Habitation contenant les informations à ajouter
     * @return L'objet Habitation créé
     *
     * @see Habitation
     */
    @Override
    public Habitation addHabitation(@NotNull Habitation habitation) {
        // Les items et les options ne sont pas enregistrés
        habitation.setItems(null);
        habitation.setOptionpayantes(null);

        return repository.save(habitation);
    }

    /** Mise à jour d'une habitation
     *
     * @param habitation Objet Habitation contenant les informations mises à jour
     * @return L'objet Habitation modifié ou null en cas d'erreur
     */
    @Override
    public Habitation updateHabitation(@NotNull Habitation habitation)
        throws HabitationException {
        // Recherche de l'habitation
        Habitation habitationUpdated = findHabitationById(habitation.getId());
        if (habitationUpdated == null) {
            throw new HabitationNotFoundException();
        }

        // Mise à jour des données de l'objet
        habitationUpdated.updateFrom(habitation);
        try {
            // Mise à jour dans la BD
            repository.save(habitationUpdated);
            return habitationUpdated;
        } catch (Exception exception) {
            throw new HabitationException(exception.getMessage());
        }
    }

    /** Suppression d'une habitation associée à l'id
     *
     * @param id Id de l’habitation
     */
    @Override
    public void deleteHabitation(@NotNull Integer id)
            throws HabitationException {
        // Recherche de l'habitation
        Habitation habitation = findHabitationById(id);
        if (habitation == null) {
            throw new HabitationNotFoundException();
        }

        try {
            // Suppression dans la BD
            deleteHabitation(habitation);
        } catch (Exception exception) {
            throw new HabitationException(exception.getMessage());
        }
    }

    private void deleteHabitation(Habitation habitation)
            throws Exception
    {
        // Suppression de toutes les liaisons habitation/item
        List<HabitationItem> items = habitationItemRepository.findHabitationItemsByHabitation_Id(habitation.getId());
        habitationItemRepository.deleteAll(items);

        // Suppression de toutes les liaisons habitation/optionpayante
        List<HabitationOptionpayante> optionpayantes = habitationOptionsRepository.findHabitationOptionpayantesByHabitation_Id(habitation.getId());
        habitationOptionsRepository.deleteAll(optionpayantes);

        // Suppression de l'habitation
        repository.delete(habitation);
    }
}
