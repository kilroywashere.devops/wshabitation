package devops.kilroywashere.wshabitation.services;

import devops.kilroywashere.wshabitation.files.HabitationFilesStorage;
import devops.kilroywashere.wshabitation.files.HabitationFilesStorageServiceImpl;
import devops.kilroywashere.wshabitation.gcp.GoogleCloudStorageValues;
import devops.kilroywashere.wshabitation.gcp.HabitationGCPStorageServiceImpl;
import devops.kilroywashere.wshabitation.models.HabitationException;
import org.springframework.stereotype.Service;

@Service
public final class HabitationFilesStorageFactory {
  private final GoogleCloudStorageValues storageValues;

  HabitationFilesStorageFactory(GoogleCloudStorageValues storageValues) {
    this.storageValues = storageValues;
  }

  public HabitationFilesStorage createHabitationFilesStorage(String filesStorageType) throws HabitationException {
    if (filesStorageType == null) {
      throw new HabitationException("Le type de storage est vide");
    }
    filesStorageType = filesStorageType.toLowerCase();

    if (filesStorageType.equals("diskfile")) {
      return new HabitationFilesStorageServiceImpl();
    }
    if (filesStorageType.equals("gcpstorage")) {
      return new HabitationGCPStorageServiceImpl(storageValues);
    }

    throw new HabitationException("Le type de storage est non reconnu");
  }
}
