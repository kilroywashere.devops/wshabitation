package devops.kilroywashere.wshabitation.services;

import devops.kilroywashere.wshabitation.files.HabitationFilesStorage;
import devops.kilroywashere.wshabitation.models.Habitation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import devops.kilroywashere.wshabitation.models.HabitationException;
import devops.kilroywashere.wshabitation.models.HabitationImage;
import devops.kilroywashere.wshabitation.models.HabitationImageId;
import devops.kilroywashere.wshabitation.repositories.HabitationImageRepository;

@Service
public class HabitationImagesServiceImpl implements HabitationImagesService {
  @Value("${wshabitation.filesStorageType:diskfile}")
  private String filesStorageType;

  private final HabitationImageRepository repository;
  private final HabitationFilesStorageFactory habitationFilesStorageFactory;
  private HabitationFilesStorage storageService;

  public HabitationImagesServiceImpl(
          HabitationImageRepository repository,
          HabitationFilesStorageFactory habitationFilesStorageFactory) {
    this.repository = repository;
    this.habitationFilesStorageFactory = habitationFilesStorageFactory;
  }

  @Override
  public Resource loadImage(String filename) throws HabitationException {
    return getStorageService().loadFileAsResource(filename);
  }

  @Override
  public HabitationImage storeImage(Habitation habitation, MultipartFile file) throws HabitationException {
    // 1 - Obtention de l'ordre maximium pour une habitation
    int ordreMax = repository.maxHabitationOrder(habitation.getId()) +1;
    // 2 - Obtention de l'extension de l'image
    String originalFilename = file.getOriginalFilename();
    assert originalFilename != null;
    String extension = originalFilename.substring(originalFilename.indexOf('.') +1);
    // 3 - Création du nom
    String newFileName = internalGetFilenameOf(habitation.getId(), ordreMax, extension);

    // 4 - Enregistrement de la resource sur le disque
    String url = getStorageService().storeFile(file, newFileName);

    // 5 - Enregistrement dans la base de données
    HabitationImageId habitationImageId = new HabitationImageId();
    habitationImageId.setHabitationId(habitation.getId());
    habitationImageId.setOrdre(ordreMax);

    HabitationImage habitationImage = new HabitationImage();
    habitationImage.setId(habitationImageId);
    habitationImage.setUrl(url);
    habitationImage.setHabitation(habitation);

    return repository.save(habitationImage);
  }

  @Override
  public void deleteImage(int habitationId, int order) {

  }

  @Override
  public void deleteImages(int habitationId) {

  }

  private String internalGetFilenameOf(int habitationId, int ordre, String extension) {
    return String.format("f%d_%dimage.%s", habitationId, ordre, extension);
  }

  private HabitationFilesStorage getStorageService() throws HabitationException {
    if (storageService == null) {
      storageService = habitationFilesStorageFactory.createHabitationFilesStorage(filesStorageType);
    }
    return storageService;
  }
}
