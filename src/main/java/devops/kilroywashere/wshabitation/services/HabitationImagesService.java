package devops.kilroywashere.wshabitation.services;

import devops.kilroywashere.wshabitation.models.Habitation;
import devops.kilroywashere.wshabitation.models.HabitationException;
import devops.kilroywashere.wshabitation.models.HabitationImage;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import java.util.List;

public interface HabitationImagesService {

  // Liste de toutes les images d'une habitation
  // List<HabitationImage> getImages(int habitationId);

  // Obtention de la resource d'une image
  Resource loadImage(@NotEmpty String filename) throws HabitationException;

  // Enregistrement d'une image
  HabitationImage storeImage(Habitation habitation, MultipartFile file) throws HabitationException;

  // Suppression d'une image
  void deleteImage(int habitationId, int order);

  // Suppression de toutes les images d'une habitation
  void deleteImages(int habitationId);
}
