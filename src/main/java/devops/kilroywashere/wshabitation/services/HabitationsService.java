package devops.kilroywashere.wshabitation.services;

import devops.kilroywashere.wshabitation.models.Habitation;
import devops.kilroywashere.wshabitation.models.HabitationException;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public interface HabitationsService {
    Iterable<Habitation> findAllHabitations();
    Iterable<Habitation> findAllHabitationsTop10();
    Iterable<Habitation> findAllHabitationsByTypeHabitatId(int id);
    Iterable<Habitation> findAllHabitationsByVilleId(String id);
    Iterable<Habitation> findAllHabitationsByIds(List<Integer> ids);
    Iterable<Habitation> findAllHabitationsByCaracteristique(Map<String, String> paramaters);

    Habitation findHabitationById(@NotNull Integer id);

    long countHabitations();

    Habitation addHabitation(@NotNull Habitation habitation);

    Habitation updateHabitation(@NotNull Habitation habitation)
            throws HabitationException;

    void deleteHabitation(@NotNull Integer id)
            throws HabitationException;
}
