package devops.kilroywashere.wshabitation.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;

@Service
public class JwtTokenService {
    @Value("${jwt.token.public_key}")
    private String publicKeyRessource;
    public Jws<Claims> validateToken(String authToken) {
        return  Jwts.parserBuilder()
                .setSigningKey(getPublicKey())
                .build()
                .parseClaimsJws(authToken);
    }
    private Key getPublicKey() {
        // Création de la clé
        byte[] publicKey = readPublicKey();
        if (publicKey != null) {
            try {
                X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKey);

                KeyFactory kf = KeyFactory.getInstance("RSA");
                return kf.generatePublic(spec);
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
        return null;
    }
    private byte[] readPublicKey() {
        return readKey(publicKeyRessource);
    }

    private static byte[] readKey(String ressourceName) {
        try {
            // Le fichier se trouve à l'extérieur du JAR
            return Files.readAllBytes(Path.of(ressourceName));
        } catch (Exception ex) {
            System.err.println("d.k.w.s.JwtTokenService.readKey, " + ex.getMessage());
        }

        return null;
    }
}
