package devops.kilroywashere.wshabitation.actuator;

import devops.kilroywashere.wshabitation.models.Habitation;
import devops.kilroywashere.wshabitation.repositories.HabitationsRepository;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component()
public class RepositoryHealthIndicator implements HealthIndicator {

    private final HabitationsRepository habitationsRepository;

    public RepositoryHealthIndicator(HabitationsRepository habitationsRepository) {
        this.habitationsRepository = habitationsRepository;
    }

    @Override
    public Health health() {
        Health.Builder status = Health.up();

        String message = "Everything ok";
        int habitationsSize = 0;
        try {
            List<Habitation> habitations = (List<Habitation>) habitationsRepository.findAll();
            habitationsSize = habitations.size();
        }
        catch (Exception exception) {
            status = Health.down();
            message = exception.getMessage();
        }

        return status
                .withDetail("Repository Habitation size", habitationsSize)
                .withDetail("Repository message", message)
                .build();
    }
}
