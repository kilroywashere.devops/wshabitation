package devops.kilroywashere.wshabitation.files;

import devops.kilroywashere.wshabitation.models.HabitationException;
import devops.kilroywashere.wshabitation.files.HabitationFilesStorage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;


public class HabitationFilesStorageServiceImpl implements HabitationFilesStorage {

  @Value("${wshabitation.images.url:}")
  private String imagesUrl;
  @Value("${wshabitation.images.resource_location:}")
  private String filePathLocation;

  public HabitationFilesStorageServiceImpl() {
  }

  @Override
  public List<String> listFiles() {
    return null;
  }

  @Override
  public Resource loadFileAsResource(String filename) throws HabitationException {
    try {
      Path file = getRootLocation().resolve(filename);
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      }
    }
    catch (Exception e) {
      throw new HabitationException("Could not read file: " + filename, e);
    }

    throw new HabitationException("Could not read file: " + filename);
  }

  @Override
  public String storeFile(MultipartFile file, String newFilename) throws HabitationException {
    if (file.isEmpty()) {
      throw new HabitationException("Failed to store empty file.");
    }

    try {
      Path rootLocation = getRootLocation();
      Path destinationFile = rootLocation.resolve(
                      Paths.get(newFilename))
              .normalize().toAbsolutePath();
      if (!destinationFile.getParent().equals(rootLocation.toAbsolutePath())) {
        // This is a security check
        throw new IOException(
                "Cannot store file outside current directory.");
      }
      try (InputStream inputStream = file.getInputStream()) {
        Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
      }
    }
    catch (IOException ioException) {
      throw new HabitationException("Failed to store file.", ioException);
    }

    return imagesUrl + "/" + newFilename;
  }

  @Override
  public boolean deleteFile(String filename) {
    return false;
  }

  private Path getRootLocation() {
    return Paths.get(filePathLocation);
  }
}
