package devops.kilroywashere.wshabitation.files;

import devops.kilroywashere.wshabitation.models.HabitationException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface HabitationFilesStorage {
  List<String> listFiles();
  Resource loadFileAsResource(String filename) throws HabitationException;
  String storeFile(MultipartFile file, String newFilename) throws HabitationException;
  boolean deleteFile(String filename) throws HabitationException;
}
