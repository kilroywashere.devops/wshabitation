package devops.kilroywashere.wshabitation.repositories;

import devops.kilroywashere.wshabitation.models.Habitation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class HabitationsRepositoryCaracteristiquesImpl implements HabitationsRepositoryCaracteristiques {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Iterable<Habitation> findAllHabitationsByCaracteristique(Map<String, String> paramaters) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Habitation> criteriaQuery = criteriaBuilder.createQuery(Habitation.class);
        Root<Habitation> root = criteriaQuery.from(Habitation.class);

        List<String> acceptedParameters = Arrays.asList("habitantsmax", "chambres", "lits", "sdb", "superficie");
        List<Predicate> predicates = new ArrayList<>();
        for (String param : acceptedParameters) {
            Integer value = getInteger(paramaters.get(param));
            if (value != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(param), value));
            }

        }

        criteriaQuery
                .select(root)
                .where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));

        Iterable<Habitation> list = entityManager.createQuery(criteriaQuery).getResultList();
        return list;
    }

    private Integer getInteger(String value) {
        Integer ret = null;
        try {
            ret = Integer.valueOf(value);
        } catch (Exception ignored) {}
        return ret;
    }
}
