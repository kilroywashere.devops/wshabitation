package devops.kilroywashere.wshabitation.repositories;

import devops.kilroywashere.wshabitation.models.Habitation;
import devops.kilroywashere.wshabitation.models.HabitationImage;
import devops.kilroywashere.wshabitation.models.HabitationImageId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface HabitationImageRepository extends CrudRepository<HabitationImage, HabitationImageId> {
  /*@Query(nativeQuery = true, value = "SELECT * FROM habitation")
  Iterable<Habitation> findTop10();*/

  @Query("select max(hi.id.ordre) from HabitationImage hi where hi.id.habitationId = ?1")
  int maxHabitationOrder(int habitationid);
}