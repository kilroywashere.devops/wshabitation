package devops.kilroywashere.wshabitation.repositories;

import devops.kilroywashere.wshabitation.models.Habitation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.validation.constraints.NotBlank;
import java.util.List;

public interface HabitationsRepository extends CrudRepository<Habitation, Integer> , HabitationsRepositoryCaracteristiques {
    List<Habitation> findHabitationsByTypehabitat_Id(int id);
    List<Habitation> findHabitationsByIdVille(@NotBlank(message = "L'id de la ville doit être renseignée") String idVille);

    List<Habitation> findHabitationsByIdIsIn(List<Integer> ids);

}


