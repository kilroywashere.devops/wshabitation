package devops.kilroywashere.wshabitation.repositories;

import devops.kilroywashere.wshabitation.models.Habitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface HabitationTop10Repository extends JpaRepository<Habitation, Integer> {
    @Query(nativeQuery = true, value = "SELECT * FROM habitation_itemscount_optionpayantecount")
    Iterable<Habitation> findTop10();
}
