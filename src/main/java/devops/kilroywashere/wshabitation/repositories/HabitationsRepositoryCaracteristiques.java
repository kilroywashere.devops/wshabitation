package devops.kilroywashere.wshabitation.repositories;

import devops.kilroywashere.wshabitation.models.Habitation;

import java.util.Map;

public interface HabitationsRepositoryCaracteristiques {
    Iterable<Habitation> findAllHabitationsByCaracteristique(Map<String, String> paramaters);
}
