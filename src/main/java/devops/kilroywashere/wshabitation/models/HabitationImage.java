package devops.kilroywashere.wshabitation.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "habitation_image")
public class HabitationImage {
    @EmbeddedId
    private HabitationImageId id;

    /*
        Json Infinite loop
        Version 2
     */
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @MapsId("habitationId")
    @JoinColumn(name = "habitation_id", nullable = false)
    private Habitation habitation;

    @Size(max = 120)
    @NotNull
    @Column(name = "url", nullable = false, length = 120)
    private String url;

    public HabitationImageId getId() {
        return id;
    }

    public void setId(HabitationImageId id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Habitation getHabitation() {
        return habitation;
    }

    public void setHabitation(Habitation habitation) {
        this.habitation = habitation;
    }
}