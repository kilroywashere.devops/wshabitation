package devops.kilroywashere.wshabitation.models;

public class HabitationException extends Exception {
    public HabitationException() {
    }

    public HabitationException(String message) {
        super(message);
    }

    public HabitationException(String message, Exception exception) {
        super(message, exception);
    }
}
