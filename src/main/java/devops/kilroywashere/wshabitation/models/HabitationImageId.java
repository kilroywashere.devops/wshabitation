package devops.kilroywashere.wshabitation.models;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class HabitationImageId implements Serializable {
    private static final long serialVersionUID = -2155809300481901231L;
    @NotNull
    @Column(name = "habitation_id", nullable = false)
    private Integer habitationId;

    @NotNull
    @Column(name = "ordre", nullable = false)
    private Integer ordre;

    public Integer getHabitationId() {
        return habitationId;
    }

    public void setHabitationId(Integer habitationId) {
        this.habitationId = habitationId;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        HabitationImageId entity = (HabitationImageId) o;
        return Objects.equals(this.ordre, entity.ordre) &&
                Objects.equals(this.habitationId, entity.habitationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ordre, habitationId);
    }

}