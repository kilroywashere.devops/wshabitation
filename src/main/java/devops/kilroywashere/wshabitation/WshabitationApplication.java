package devops.kilroywashere.wshabitation;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "WS Habitations",
                version = "1.0",
                description = "Gestion des habitations de l'application RentAHouse",
                contact = @Contact(email = "kilroywashere.devops@gmail.com", name = "Jean-Luc BOMPARD AKA Kilroy")
        )
)
public class WshabitationApplication {

    public static void main(String[] args) {
        SpringApplication.run(WshabitationApplication.class, args);
    }

    /** Autorisation CORS
     *
     * @return WebMvcConfigurer
     */
    @Bean
    public WebMvcConfigurer corsConfigurer()
    {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowCredentials(true)
                        .allowedOriginPatterns("*")
                        .allowedHeaders("*")
                        .allowedMethods("OPTIONS", "GET", "POST", "PUT", "DELETE");
            }
        };
    }
}
