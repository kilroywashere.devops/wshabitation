package devops.kilroywashere.wshabitation.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] WHITELIST = {
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**",
            // -- Actuator
            "/actuator/**"

    };

    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() {
        return new JwtAuthenticationTokenFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(WHITELIST).permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/typehabitats/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/typehabitats/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/optionpayantes/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/optionpayantes/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/items/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/items/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/habitations/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/habitations/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/habitationoptionpayantes/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/habitationoptionpayantes/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/api/v1/habitationimages/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/habitationimages/**").permitAll()
                .anyRequest().authenticated();
    }
}
