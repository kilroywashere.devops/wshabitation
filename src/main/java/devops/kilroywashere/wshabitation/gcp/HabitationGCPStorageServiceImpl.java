package devops.kilroywashere.wshabitation.gcp;

import devops.kilroywashere.wshabitation.files.HabitationFilesStorage;
import devops.kilroywashere.wshabitation.models.HabitationException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public class HabitationGCPStorageServiceImpl implements HabitationFilesStorage {
  private final GoogleCloudStorageUtils googleCloudStorageUtils;

  public HabitationGCPStorageServiceImpl(GoogleCloudStorageValues storageValues) throws HabitationException {
    try {
      this.googleCloudStorageUtils = new GoogleCloudStorageUtils(storageValues);
    } catch (Exception e) {
      throw new HabitationException("Impossible de créer l'objet GoogleCloudStorageUtils");
    }
  }


  @Override
  public List<String> listFiles() {
    return googleCloudStorageUtils.listFiles();
  }

  @Override
  public Resource loadFileAsResource(String filename) throws HabitationException {
    try {
      return googleCloudStorageUtils.readFile(filename);
    } catch (Exception e) {
      throw new HabitationException("Impossible de lire le fichier");
    }
  }

  @Override
  public String storeFile(MultipartFile file, String newFilename) throws HabitationException {
    if (file.isEmpty()) {
      throw new HabitationException("Failed to store empty file.");
    }

    try {
      return googleCloudStorageUtils.uploadFile(file, newFilename);
    } catch (Exception exception) {
      throw new HabitationException("Failed to store file.", exception);
    }
  }

  @Override
  public boolean deleteFile(String filename) throws HabitationException {
    try {
      return googleCloudStorageUtils.deleteFile(filename);
    } catch (Exception exception) {
      throw new HabitationException("Failed to delete file.", exception);
    }
  }
}
