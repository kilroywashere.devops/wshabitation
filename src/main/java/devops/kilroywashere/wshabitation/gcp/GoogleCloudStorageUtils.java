package devops.kilroywashere.wshabitation.gcp;

import com.google.cloud.storage.*;
import com.google.api.gax.paging.Page;
import com.google.auth.Credentials;
import com.google.auth.oauth2.ServiceAccountCredentials;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GoogleCloudStorageUtils {

  private final GoogleCloudStorageValues storageValues;
  private final Storage storage;

  public GoogleCloudStorageUtils(GoogleCloudStorageValues storageValues) throws IOException {
    this.storageValues = storageValues;

    String filePath = storageValues.getKeyFilePath();
    assert filePath.startsWith("file:");

    Credentials credentials = ServiceAccountCredentials
            .fromStream(new FileInputStream(filePath.substring(5)));
    // le project Id est fixé par les credentials
    storage = StorageOptions.newBuilder().setCredentials(credentials)
            .build().getService();
  }

  public List<String> listFiles() {
    List<String> files = new ArrayList<>();
    Page<Blob> blobs = storage.list(storageValues.getBucketId());
    for (Blob blob : blobs.iterateAll()) {
      files.add(getFileUrl(blob.getName()));
    }
    return files;
  }

  public ByteArrayResource readFile(String filename) {
    byte[] content = storage.readAllBytes(storageValues.getBucketId(), filename);
    return new ByteArrayResource(content);
  }

  public String uploadFile(MultipartFile file, String filename) throws IOException {
    if (file == null) {
      throw new IOException("Le fichier n'existe pas");
    }
    try {
      BlobId blobId = BlobId.of(storageValues.getBucketId(), filename);
      BlobInfo blobInfo = BlobInfo.newBuilder(blobId)
              .setContentType(file.getContentType()).build();
      storage.create(blobInfo, file.getBytes());

      // URL d'accès
      return getFileUrl(filename);
    } catch (Exception e) {
      throw new IOException("Impossible de lire le fichier");
    }
  }

  public boolean deleteFile(String filename) {
    Blob blob = storage.get(BlobId.of(storageValues.getBucketId(), filename));
    return blob.delete();
  }

  private String getFileUrl(String filename) {
    return String.format("%s/%s/%s", storageValues.getUrl(), storageValues.getBucketId(), filename);
  }
}
