package devops.kilroywashere.wshabitation.gcp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoogleCloudStorageValues {

  @Value("${google.storage.rhstorage.bucketid:}")
  private String bucketId;
  @Value("${google.storage.rhstorage.url:}")
  private String url;
  @Value("${spring.cloud.gcp.credentials.location:}")
  private String keyFilePath;

  public String getBucketId() {
    return bucketId;
  }

  public String getKeyFilePath() {
    return keyFilePath;
  }

  public String getUrl() {
    return url;
  }
}