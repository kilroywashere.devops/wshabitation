package devops.kilroywashere.wshabitation.controller;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import devops.kilroywashere.wshabitation.models.Habitation;
import devops.kilroywashere.wshabitation.models.HabitationException;
import devops.kilroywashere.wshabitation.models.HabitationNotFoundException;
import devops.kilroywashere.wshabitation.services.HabitationsService;


@RestController
@RequestMapping("/api/v1")
public class HabitationsController {
    private final HabitationsService service;
    /**
     * Métriques Micrometer
     */
    Counter habitationsGetAllCount;

    /**
     * Logging
     */
    private final Logger logger = LoggerFactory.getLogger(HabitationsController.class);

    // DI par Spring Boot
    public HabitationsController(
            HabitationsService service,
            MeterRegistry registry
    ) {
        this.service = service;
        Gauge
                .builder("HabitationsController.HabitationsCount", fetchHabitationsCount())
                .tag("version", "v1")
                .description("Nombre d'habitations dans la BDD")
                .register(registry);
        //habitationsGetAllCount = registry.counter("habitations_GetAll_Count");
        habitationsGetAllCount = Counter.builder("habitations_GetAll_Count")
                .description("Total de requête pour la méthode getHabitations")
                .register(registry);
    }
    public Supplier<Number> fetchHabitationsCount() {
        return service::countHabitations;
    }

    /**
     * Retourne toutes les habitations
     *
     * @return Une liste d'objet Habitation
     */
    @GetMapping("/habitations")
    @ResponseStatus(HttpStatus.OK)
    @Timed(value = "habitations.getall.time", description = "Durée pour obtenir toutes les habitations", percentiles = {0.5,0.9})
    public Iterable<Habitation> getHabitations() {
        if (habitationsGetAllCount != null)
            habitationsGetAllCount.increment();
        return service.findAllHabitations();
    }


    /**
     * Retourne toutes les habitations
     *
     * @return Une liste d'objet Habitation
     */
    @GetMapping("/habitations/top10")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Habitation> getHabitationsTop10() {
        return service.findAllHabitationsTop10();
    }

    /**
     * Retourne toutes les habitations d'un type d'habitat'
     * @param id Id du type d'habitat
     * @return Une liste d'objet Habitation
     */
    @Operation(summary = "Recherche d'habitations par type d'habitat (maison, appartement)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Recherche d'habitations",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Habitation.class)) }),
    })
    @GetMapping("/habitations/typehabitat/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Habitation> getHabitationsByTypeHabitatId(@PathVariable int id) {
        return service.findAllHabitationsByTypeHabitatId(id);
    }

    /**
     * Retourne toutes les habitations d'une ville
     *
     * @param id Id de la ville
     * @return Une liste d'objet Habitation
     */
    @GetMapping("/habitations/ville/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Habitation> getHabitationsByVilleId(@PathVariable String id) {
        return service.findAllHabitationsByVilleId(id);
    }

    /**
     * Retourne toutes les habitations d'après une liste d'ids
     *
     * @param ids Liste des id des habitations à rechercher
     * @return Une liste d'objet Habitation
     */
    @GetMapping("/habitations/ids/{ids}")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Habitation> getHabitationsByIds(@PathVariable String ids) {
        List<Integer> list =
            Arrays.stream(ids.split(",")).map(Integer::valueOf).toList();
        return service.findAllHabitationsByIds(list);
    }

    // Voir https://www.baeldung.com/spring-request-param">Spring @RequestParam Annotation
    /**
     * Retourne toutes les habitations ayant les caractéristques données
     * Seules les caractéristiques suivantes sont valides :
     * - habitants : habitants minimum
     * - chambres : habitants minimum
     * - lits : lits minimum
     * - sdb : salles de bains minimum
     * - superficie : superficie minimum
     *
     * @return Une liste d'objet Habitation
     */
    @GetMapping("/habitations/caracteristiques")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Habitation> getHabitationsByCarateristiques(@RequestParam Map<String, String> paramaters) {
        return service.findAllHabitationsByCaracteristique(paramaters);
    }

    /** Retourne l'habitation associée à l'id ou une erreur si non trouvée
     * Méthode REST GET publique
     *
     * @param id Id de l’habitation
     * @return L'objet Habitation correspondant à l'id ou une erreur ResponseStatusException
     *         Le status HTTP NOT_FOUND si l'habitation n'existe pas
     */
    @Operation(summary = "Recherche de bien avec l'id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Recherche de biens",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Habitation.class)) }),
            @ApiResponse(responseCode = "404", description = "L'habitation ayant l'id n'existe pas",
                    content = @Content) })
    @GetMapping("/habitations/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Habitation getHabitation(@PathVariable(value = "id") Integer id) {
        Habitation habitation = service.findHabitationById(id);

        if (habitation == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("L'habitation ayant l'id (%d) n'existe pas", id)
            );
        }

        return habitation;
    }

    /** Ajoute une habitation
     * @implNote Les items et les options ne sont pas enregistrés par cette API
     *
     * @param habitation Objet Habitation contenant les informations à ajouter
     * @return Le status HTTP 201 : Created avec l'url de la ressource créée si tout est ok
     *        Le status HTTP BAD_REQUEST sinon
     * @see Habitation
     */
    @Operation(summary = "Ajoute une habitation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Ajout de l'habitation",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Habitation.class)) }),
            @ApiResponse(responseCode = "400", description = "L'habitation contient des valeurs invalides",
                    content = @Content) })
    @Secured(value = "ROLE_ADMIN")
    @PostMapping("/habitations")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Habitation> addHabitation(@Validated @RequestBody Habitation habitation) {
        try {
            Habitation habitationCreated = service.addHabitation(habitation);

            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequestUri()
                    .path("/{id}")
                    .buildAndExpand(habitationCreated.getId())
                    .toUri();
            return ResponseEntity.created(location).body(habitationCreated);
        } catch (Exception exception) {
            logger.error("HabitationsController", exception);

            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("Habitation: %s", habitation.toString())
            );
        }
    }

    /** Mise à jour d'une habitation
     *
     * @param id Id de l’habitation
     * @param habitation Objet Habitation contenant les informations mises à jour
     * @return Le status HTTP 200 si tout est ok
     *         Le status HTTP NOT_FOUND si l'habitation n'existe pas
     *           ou BAD_REQUEST sinon
     */
    @Secured(value = "ROLE_ADMIN")
    @PutMapping("/habitations/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Habitation updateHabitation(@PathVariable Integer id, @Validated @RequestBody Habitation habitation) {
        // Vérification des paramètres
        if (! id.equals(habitation.getId())) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "L'id de l'URL ne correspond pas à l'id du body"
            );
        }

        Habitation habitationUpdated;
        try {
            habitationUpdated = service.updateHabitation(habitation);
        } catch (HabitationNotFoundException exception) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("L'habitation ayant l'id (%d) n'existe pas", id)
            );
        } catch (HabitationException exception) {
            logger.error("HabitationsController", exception);

            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("Habitation: %s", habitation)
            );
        }

        // HTTP Status Code 200 (Ok) par défaut
        logger.info(String.format("Habitation mise à jour: %s", habitationUpdated));
        return habitationUpdated;
    }

    /** Supprime une habitation
     *
     * @param id L'id de l'habitation à supprimer
     * @return HTTP Status Code 204 NO CONTENT (succès), Code 404 (ressource non trouvée)
     *              ou Code 500 Erreur serveur
     */
    @Secured(value = "ROLE_ADMIN")
    @DeleteMapping("/habitations/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteHabitation(@PathVariable Integer id) {
        try {
            service.deleteHabitation(id);
            logger.info(String.format("Habitation ayant l'id (%d)  a été supprimée", id));

            // HTTP Status Code 204 (NO CONTENT)
            return ResponseEntity.noContent().build();
        } catch (HabitationNotFoundException exception) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("L'habitation ayant l'id (%d) n'existe pas", id)
            );
        } catch (HabitationException exception) {
            logger.error("HabitationsController", exception);

            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    String.format("Habitation ayant l'id (%d) n'a pas pu être supprimée", id)
            );
        }
    }
}
