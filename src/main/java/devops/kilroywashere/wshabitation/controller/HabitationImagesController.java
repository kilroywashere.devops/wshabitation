package devops.kilroywashere.wshabitation.controller;

import javax.validation.constraints.NotEmpty;
import java.net.URI;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import devops.kilroywashere.wshabitation.services.HabitationImagesService;
import devops.kilroywashere.wshabitation.models.Habitation;
import devops.kilroywashere.wshabitation.models.HabitationImage;
import devops.kilroywashere.wshabitation.services.HabitationsService;



@RestController
@RequestMapping("/api/v1")
public class HabitationImagesController {
  private final HabitationImagesService service;
  private final HabitationsService habitationsService;

  /**
   * Logging
   */
  private final Logger logger = LoggerFactory.getLogger(HabitationsController.class);

  public HabitationImagesController(HabitationImagesService service, HabitationsService habitationsService) {
    this.service = service;
    this.habitationsService = habitationsService;
  }

  @ApiResponses(value = {
          @ApiResponse(responseCode = "200", description = "Obtention d'une image d'une habitation",
                  content = { @Content(mediaType = "application/json",
                          schema = @Schema(implementation = Resource.class)) }),
          @ApiResponse(responseCode = "400", description = "L'image n'a pas pu être chargée",
                  content = @Content) })
  @GetMapping("/habitationimages/{filename}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Resource> getImage(@NotEmpty @PathVariable String filename) {
    try {
      Resource file = service.loadImage(filename);

      return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
              "attachment; filename=\"" +
              file.getFilename() + "\"").body(file);
    } catch (Exception exception) {
      logger.error("HabitationImagesController", exception);

      throw new ResponseStatusException(
              HttpStatus.BAD_REQUEST,
              String.format("HabitationImage-filename: %s", filename)
      );
    }
  }

  @Operation(summary = "Ajoute une image pour une habitation")
  @ApiResponses(value = {
          @ApiResponse(responseCode = "201", description = "Ajout de l'image",
                  content = { @Content(mediaType = "application/json",
                          schema = @Schema(implementation = HabitationImage.class)) }),
          @ApiResponse(responseCode = "400", description = "L'image pour l'habitation est invalide",
                  content = @Content) })
  @Secured(value = "ROLE_ADMIN")
  @PostMapping("/habitationimages/{habitationId}")
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<HabitationImage> addImageHabitation(@NotEmpty @PathVariable int habitationId, @RequestParam("file") MultipartFile file) {
    Habitation habitation = habitationsService.findHabitationById(habitationId);
    if (habitation == null) {
      throw new ResponseStatusException(
              HttpStatus.NOT_FOUND,
              String.format("L'habitation ayant l'id (%d) n'existe pas", habitationId)
      );
    }

    try {
      HabitationImage habitationImage = service.storeImage(habitation, file);
      String url = habitationImage.getUrl();
      String filename = url.substring(url.lastIndexOf('/') +1);

      URI location = ServletUriComponentsBuilder
              .fromCurrentRequestUri()
              .path("/{filename}")
              .buildAndExpand(filename)
              .toUri();
      return ResponseEntity.created(location).body(habitationImage);
    } catch (Exception exception) {
      logger.error("HabitationImagesController", exception);

      throw new ResponseStatusException(
              HttpStatus.BAD_REQUEST,
              String.format("Image pour habitationId: %d", habitationId)
      );
    }
  }
}
