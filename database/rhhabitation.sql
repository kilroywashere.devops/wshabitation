-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : lun. 21 nov. 2022 à 12:23
-- Version du serveur : 8.0.31-0ubuntu0.22.04.1
-- Version de PHP : 8.1.2-1ubuntu2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `rhhabitation`
--

-- --------------------------------------------------------

--
-- Structure de la table `habitation`
--

CREATE TABLE `habitation` (
  `id` int NOT NULL,
  `typehabitat_id` int NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `adresse` varchar(100) NOT NULL,
  `id_ville` int NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `habitantsmax` int NOT NULL,
  `chambres` int NOT NULL,
  `lits` int NOT NULL,
  `sdb` int NOT NULL,
  `superficie` int NOT NULL,
  `prixnuit` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Déchargement des données de la table `habitation`
--

INSERT INTO `habitation` (`id`, `typehabitat_id`, `libelle`, `description`, `adresse`, `id_ville`, `image`, `habitantsmax`, `chambres`, `lits`, `sdb`, `superficie`, `prixnuit`) VALUES
(1, 1, 'Maison provençale', 'Une description', '12 Rue du Coq qui chante', 56, 'maison.png', 8, 3, 3, 1, 92, '60'),
(2, 2, 'Appartement centre ville', 'bla bla', 'Rue du centre', 99, 'appartement.png', 4, 1, 2, 1, 50, '56'),
(3, 1, 'Maison 3', 'Desc Maison 3', 'Rue 3', 4, 'maison.png', 5, 2, 3, 1, 100, '50'),
(4, 1, 'Maison 4', 'Desc Maison 4', 'Rue 4', 4, 'maison.png', 5, 2, 3, 1, 100, '50'),
(5, 1, 'Maison 5', 'Desc Maison 5', 'Rue 5', 4, 'maison.png', 6, 3, 4, 1, 120, '60'),
(6, 1, 'Maison 6', 'Desc Maison 6', 'Rue 6', 4, 'maison.png', 5, 2, 3, 1, 100, '50'),
(7, 1, 'Maison 7', 'Desc Maison 7', 'Rue 7', 4, 'maison.png', 6, 3, 4, 2, 120, '60'),
(8, 1, 'Maison 8', 'Desc Maison 8', 'Rue 8', 4, 'maison.png', 5, 2, 3, 1, 100, '50'),
(9, 1, 'Maison 9', 'Desc Maison 9', 'Rue 9', 4, 'maison.png', 6, 3, 4, 1, 120, '60'),
(10, 1, 'Maison 10', 'Desc Maison 10', 'Rue 10', 4, 'maison.png', 5, 2, 3, 1, 100, '50'),
(11, 2, 'Appartement 4', 'Desc Appartement 4', 'Rue 4', 3, 'appartement.png', 5, 2, 3, 1, 70, '35'),
(12, 2, 'Appartement 5', 'Desc Appartement 5', 'Rue 5', 3, 'appartement.png', 6, 3, 3, 1, 80, '40'),
(13, 2, 'Appartement 6', 'Desc Appartement 6', 'Rue 6', 3, 'appartement.png', 5, 2, 3, 1, 70, '35'),
(14, 2, 'Appartement 7', 'Desc Appartement 7', 'Rue 7', 3, 'appartement.png', 6, 3, 3, 1, 80, '40'),
(15, 2, 'Appartement 8', 'Desc Appartement 8', 'Rue 8', 3, 'appartement.png', 5, 2, 3, 1, 70, '35'),
(16, 2, 'Appartement 9', 'Desc Appartement 9', 'Rue 9', 3, 'appartement.png', 6, 3, 3, 1, 80, '40'),
(17, 2, 'Appartement 10', 'Desc Appartement 10', 'Rue 10', 3, 'appartement.png', 5, 2, 3, 1, 70, '35');

-- --------------------------------------------------------

--
-- Structure de la table `habitation_item`
--

CREATE TABLE `habitation_item` (
  `habitation_id` int NOT NULL,
  `item_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `habitation_item`
--

INSERT INTO `habitation_item` (`habitation_id`, `item_id`) VALUES
(1, 1),
(2, 1),
(1, 2),
(4, 3),
(7, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3);

-- --------------------------------------------------------

--
-- Structure de la table `habitation_optionpayante`
--

CREATE TABLE `habitation_optionpayante` (
  `habitation_id` int NOT NULL,
  `optionpayante_id` int NOT NULL,
  `prix` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `habitation_optionpayante`
--

INSERT INTO `habitation_optionpayante` (`habitation_id`, `optionpayante_id`, `prix`) VALUES
(1, 1, 60),
(1, 2, 30),
(1, 3, 20),
(2, 1, 40),
(5, 1, 50),
(5, 2, 40),
(11, 1, 40),
(12, 1, 50),
(12, 2, 20),
(13, 1, 40),
(13, 2, 30),
(14, 1, 50),
(14, 2, 20),
(15, 1, 40),
(15, 2, 30),
(16, 1, 50),
(16, 2, 20),
(17, 1, 40),
(17, 2, 30);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE `item` (
  `id` int NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`id`, `libelle`, `description`) VALUES
(1, 'Internet', 'Wifi et Fibre'),
(2, 'Lac', 'Base de loisirs à 2 km'),
(3, 'Climatisation', 'Climatisation réversible');

-- --------------------------------------------------------

--
-- Structure de la table `locationro`
--

CREATE TABLE `locationro` (
  `id` int NOT NULL,
  `idutilisateur` int NOT NULL,
  `habitation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `locationro`
--

INSERT INTO `locationro` (`id`, `idutilisateur`, `habitation_id`) VALUES
(1, 5, 1),
(2, 6, 2);

-- --------------------------------------------------------

--
-- Structure de la table `optionpayante`
--

CREATE TABLE `optionpayante` (
  `id` int NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `optionpayante`
--

INSERT INTO `optionpayante` (`id`, `libelle`, `description`) VALUES
(1, 'Ménage', 'A la fin du séjour'),
(2, 'Drap de lit', 'Pour l\'ensemble des lits'),
(3, 'Linge de maison', 'Linge de toilette pour la salle de bain');

-- --------------------------------------------------------

--
-- Structure de la table `typehabitat`
--

CREATE TABLE `typehabitat` (
  `id` int NOT NULL,
  `libelle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `typehabitat`
--

INSERT INTO `typehabitat` (`id`, `libelle`) VALUES
(1, 'Maison'),
(2, 'Appartement');

-- --------------------------------------------------------

--
-- Structure de la vue `habitation_itemscount_optionpayantecount`
--
DROP TABLE IF EXISTS `habitation_itemscount_optionpayantecount`;

CREATE ALGORITHM=UNDEFINED DEFINER=`adminrh`@`%` SQL SECURITY DEFINER VIEW `habitation_itemscount_optionpayantecount`  AS SELECT `habitation`.`id` AS `id`, `habitation`.`typehabitat_id` AS `typehabitat_id`, `habitation`.`libelle` AS `libelle`, `habitation`.`description` AS `description`, `habitation`.`adresse` AS `adresse`, `habitation`.`id_ville` AS `id_ville`, `habitation`.`image` AS `image`, `habitation`.`habitantsmax` AS `habitantsmax`, `habitation`.`chambres` AS `chambres`, `habitation`.`lits` AS `lits`, `habitation`.`sdb` AS `sdb`, `habitation`.`superficie` AS `superficie`, `habitation`.`prixnuit` AS `prixnuit` FROM ((`habitation` left join (select `habitation`.`id` AS `itemhabitationid`,count(`habitation_item`.`item_id`) AS `ItemsCount` from (`habitation` join `habitation_item`) where (`habitation`.`id` = `habitation_item`.`habitation_id`) group by `habitation`.`id`) `itemsCount` on((`habitation`.`id` = `itemsCount`.`itemhabitationid`))) left join (select `habitation`.`id` AS `optionhabitationid`,count(`habitation_optionpayante`.`optionpayante_id`) AS `OptionsPayantesCount` from (`habitation` join `habitation_optionpayante`) where (`habitation`.`id` = `habitation_optionpayante`.`habitation_id`) group by `habitation`.`id`) `optionsPayantesCount` on((`habitation`.`id` = `optionsPayantesCount`.`optionhabitationid`))) ORDER BY `itemsCount`.`ItemsCount` DESC, `optionsPayantesCount`.`OptionsPayantesCount` DESC LIMIT 0, 10 ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `habitation`
--
ALTER TABLE `habitation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_typehabitat` (`typehabitat_id`);

--
-- Index pour la table `habitation_item`
--
ALTER TABLE `habitation_item`
  ADD PRIMARY KEY (`habitation_id`,`item_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Index pour la table `habitation_optionpayante`
--
ALTER TABLE `habitation_optionpayante`
  ADD PRIMARY KEY (`habitation_id`,`optionpayante_id`),
  ADD KEY `optionpayante_id` (`optionpayante_id`);

--
-- Index pour la table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `locationro`
--
ALTER TABLE `locationro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `habitation_id` (`habitation_id`);

--
-- Index pour la table `optionpayante`
--
ALTER TABLE `optionpayante`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typehabitat`
--
ALTER TABLE `typehabitat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `habitation`
--
ALTER TABLE `habitation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `item`
--
ALTER TABLE `item`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `optionpayante`
--
ALTER TABLE `optionpayante`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `typehabitat`
--
ALTER TABLE `typehabitat`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `habitation`
--
ALTER TABLE `habitation`
  ADD CONSTRAINT `habitation_ibfk_1` FOREIGN KEY (`typehabitat_id`) REFERENCES `typehabitat` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `habitation_item`
--
ALTER TABLE `habitation_item`
  ADD CONSTRAINT `habitation_item_ibfk_1` FOREIGN KEY (`habitation_id`) REFERENCES `habitation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `habitation_item_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `habitation_optionpayante`
--
ALTER TABLE `habitation_optionpayante`
  ADD CONSTRAINT `habitation_optionpayante_ibfk_1` FOREIGN KEY (`habitation_id`) REFERENCES `habitation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `habitation_optionpayante_ibfk_2` FOREIGN KEY (`optionpayante_id`) REFERENCES `optionpayante` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `locationro`
--
ALTER TABLE `locationro`
  ADD CONSTRAINT `locationro_ibfk_1` FOREIGN KEY (`habitation_id`) REFERENCES `habitation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
